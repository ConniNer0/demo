import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { OverallComponent } from './overview/overall/overall.component';
import { InstitutionComponent } from './overview/institution/institution.component';
import { TraineeComponent } from './overview/trainee/trainee.component';
import { AuthGuard } from '../core/auth/auth.guard';


const routes: Routes = [
  {
    path: 'overview',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
        {
            path: '',
            component: OverallComponent
        },
        {
            path: ':institution',
            component: InstitutionComponent
        },
        {
            path: 'trainee/:trainee',
            component: TraineeComponent
        }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
