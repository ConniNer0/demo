import { NgModule } from '@angular/core';
import { MainComponent } from './main.component';
import { SharedModule } from '../shared/shared.module';
import { OverallComponent } from './overview/overall/overall.component';
import { InstitutionComponent } from './overview/institution/institution.component';
import { TraineeComponent } from './overview/trainee/trainee.component';
import { MainRoutingModule } from './main-routing.module';
import { ChartsModule } from 'ng2-charts';
import { GoogleChartsModule } from 'angular-google-charts';


@NgModule({
  declarations: [MainComponent, OverallComponent, InstitutionComponent, TraineeComponent],
  imports: [
    SharedModule,
    MainRoutingModule,
    ChartsModule,
    GoogleChartsModule
  ]
})
export class MainModule { }
