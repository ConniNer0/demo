import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Overall } from 'src/app/models/overall.modes';
import { OverallService } from 'src/app/services/main/overall.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-overall',
  templateUrl: './overall.component.html',
  styleUrls: ['./overall.component.scss']
})
export class OverallComponent implements OnInit {

  schoolFilterList: string[] = [];
  overallDataSource: MatTableDataSource<Overall>;

  capacityOverall: number = 0;
  capacityOverall_max: number = 0;
  capacityPercent: number = 0;

  columnsToDisplay = ['name', 'capacity'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private overallService: OverallService,
    private router: Router) { }

  ngOnInit(): void {
    this.overallService.getOverallData().then(overallData => {
      overallData.forEach(data => {
        this.capacityOverall += data.capacity;
        this.capacityOverall_max += data.capacity_max;
      });
      this.capacityPercent = this.getPercentCapacity(this.capacityOverall, this.capacityOverall_max);
      this.overallDataSource = new MatTableDataSource(overallData);
      this.overallDataSource.paginator = this.paginator;
      this.overallDataSource.sort = this.sort;
    });
  }

  addSchoolFilter(schoolFilter: string): void {
    if (!(this.schoolFilterList.find(filter => filter == schoolFilter))) {
      this.schoolFilterList.push(schoolFilter);
    }
  }

  removeSchoolFilter(schoolFilter: string): void {
    this.schoolFilterList = this.schoolFilterList.filter(filter => filter !== schoolFilter);
  }

  showInstitution(data: Overall) {
    this.router.navigate(['overview/' + data.id]);
  }

  getPercentCapacity(capacity: number, capacity_max: number): number {
    return Math.round(((capacity / capacity_max) * 100) * 10) / 10;
  }

}
