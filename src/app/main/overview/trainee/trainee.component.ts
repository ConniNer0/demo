import { Component, OnInit } from '@angular/core';
import { Trainee } from 'src/app/models/trainee.model';
import { TraineeService } from 'src/app/services/main/trainee/trainee.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-trainee',
  templateUrl: './trainee.component.html',
  styleUrls: ['./trainee.component.scss']
})
export class TraineeComponent implements OnInit {

  trainee: Trainee;

  constructor(private traineeService: TraineeService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    console.log(this.activatedRoute.snapshot.params.trainee);
    this.traineeService.getTrainee(this.activatedRoute.snapshot.params.trainee).then(trainee => {
      this.trainee = trainee;
    });
  }


}
