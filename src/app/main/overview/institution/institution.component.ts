import { Component, OnInit, ViewChild } from '@angular/core';
import { InstitutionService } from 'src/app/services/main/institution/institution.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { Trainee } from 'src/app/models/trainee.model';
import { Institution } from 'src/app/models/institution.model';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import * as moment from 'moment';

@Component({
  selector: 'app-institution',
  templateUrl: './institution.component.html',
  styleUrls: ['./institution.component.scss']
})
export class InstitutionComponent implements OnInit {

  institution: Institution;
  traineeDataSource: MatTableDataSource<Trainee>;

  barLabelNames: string[] = [];

  myData = [];
  chartColumns = ['Name', 'Start', 'End'];

  columnsToDisplay = ['name', 'startDate', 'endDate', 'daysDiff'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private institutionService: InstitutionService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.institutionService.getInstitution(this.activatedRoute.snapshot.params.institution).then(institution => {
      this.institution = institution;
      institution.trainees.forEach(trainee => {
        this.myData.push([trainee.name, new Date(trainee.startDate), new Date(trainee.endDate)]);
      });
      this.barLabelNames = this.institution.trainees.map(trainee => trainee.name);
      this.traineeDataSource = new MatTableDataSource(institution.trainees);
      this.traineeDataSource.paginator = this.paginator;
      this.traineeDataSource.sort = this.sort;
    });
  }

  getPercentCapacity(capacity: number, capacity_max: number): number {
    return Math.round(((capacity / capacity_max) * 100) * 10) / 10;
  }

  remainingDays(p_endDate: Date): number {
    const today = moment(new Date());
    const endDate = moment(new Date(p_endDate));
    return endDate.diff(today, 'days');
  }

  showTrainee(data: Institution) {
    this.router.navigate(['overview/trainee/' + data.id]);
  }



  public barChartOptions = {
    scaleShowVerticalLines: true,
    responsive: true
  };
  public barChartLabels = ['September', 'Oktober', 'November', 'Dezember'];
  public barChartType = 'horizontalBar';
  public barChartLegend = true;
  public barChartData = [
    {data: [{x:'4', y:'September'}, {x: '4', y: 'Oktober'},{x: '4', y: 'November'},{x: '4', y: 'Dezember'} ], label: 'Auslastung'}
  ];

}
