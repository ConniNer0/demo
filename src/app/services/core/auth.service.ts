import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  isLoggedIn(): boolean {
    if (localStorage.getItem('user')) return true;
    else return false;
  }

  login(): boolean {
    localStorage.setItem('user', 'firstUser');
    return true;
  }

  logout(): boolean {
    localStorage.removeItem('user');
    return true;
  }
}
