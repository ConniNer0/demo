import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Trainee } from 'src/app/models/trainee.model';
import { Institution } from 'src/app/models/institution.model';

@Injectable({
  providedIn: 'root'
})
export class TraineeService {

  constructor(private httpClient: HttpClient) { }

  public getTrainee(id: string): Promise<Trainee> {
    return this.httpClient.get<Trainee[]>('assets/einrichtung_solo.json').toPromise().then((inistitutions: Institution[]) => {
      let traineeTemp: Trainee;
      inistitutions.forEach(inistitution => {
        inistitution.trainees.forEach(trainee => {
          if(trainee.id === id) {
            traineeTemp = trainee;
            traineeTemp.institution = inistitution.name;
          }
        });
      });
      return traineeTemp;
    });
  }
}
