import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Institution } from 'src/app/models/institution.model';

@Injectable({
  providedIn: 'root'
})
export class InstitutionService {

  constructor(private httpClient: HttpClient) { }

  public getInstitution(id: string): Promise<Institution> {
    return this.httpClient.get<Institution[]>('assets/einrichtung_solo.json').toPromise().then((inistitutions: Institution[]) => {
      return inistitutions.find(institution => institution.id === id);
    });
  }
}
