import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Overall } from 'src/app/models/overall.modes';

@Injectable({
  providedIn: 'root'
})
export class OverallService {

  constructor(private httpClient: HttpClient) { }

  public getOverallData(): Promise<Overall[]> {
    return this.httpClient.get<Overall[]>('/assets/einrichtungen.json').toPromise();
  }
}
