export interface IOverall {
    id?: string;
    name?: string;
    capacity?: number;
    capacity_max?: number;
}

export class Overall implements IOverall {
    constructor(
        public id?: string,
        public name?: string,
        public capacity?: number,
        public capacity_max?: number
    ) {}
}
