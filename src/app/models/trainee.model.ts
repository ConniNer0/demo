export interface ITrainee {
    id?: string;
    name?: string;
    startDate?: Date;
    endDate?: Date;
    school?: string;
    institution?: string;
}

export class Trainee implements ITrainee {
    constructor(
        public id?: string,
        public name?: string,
        public startDate?: Date,
        public endDate?: Date,
        public school?: string,
        public institution?: string
    ) {}
}