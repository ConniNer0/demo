import { Trainee } from './trainee.model';

export interface IInstitution {
    id?: string;
    name?: string;
    capacity?: number;
    capacity_max?: number;
    trainees?: Trainee[];
}

export class Institution implements IInstitution {
    constructor(
        public id?: string,
        public name?: string,
        public capacity?: number,
        public capacity_max?: number,
        public trainees?: Trainee[]
    ) {}
}