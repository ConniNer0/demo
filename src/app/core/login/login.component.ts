import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/core/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  passwordVisible = false;
  loginFormGroup: FormGroup;

  readonly email = new FormControl('deine@e-mail.com', [Validators.required]);
  readonly password = new FormControl('Passwort1', [Validators.required]);

  constructor(private router: Router,
    private authService: AuthService) {
      if(this.authService.isLoggedIn()) {
        this.router.navigate(['overview']);
      }
    }

  ngOnInit(): void {
    this.loginFormGroup = new FormGroup({
      email: this.email,
      password: this.password
    });
  }

  onFormSubmit(): void {
   if (this.authService.login()) {
     this.router.navigate(['overview']);
   }
  }

}
